# Installation

## Considerations

The easiest way to start using Tonalities is by using our [online instance](https://data-iremus.huma-num.fr/tonalities) hosted at Huma-Num. To run Tonalities locally, see next section.

Tonalities architecture relies on 3 pilars : client webapp, auth server & tripletore database.
## Install locally

### Prerequistes
You need Jena Fuseki running with a custom graph created called `tonalities`. See official documentation.

### Configure authentification
Go to https://orcid.org/developer-tools to register for your ORCID Public API credentials.

In Application Url, type `https://my-domain.com/sherlock`
And Redirect URIs, type `https://my-domain.com/sherlock/login/redirect`
You will then get your Client ID and Client secret.

### Server-side
Clone the sherlock-service repository and edit the config file by adding missing variables to link to your OrcID pro account.
```
./start.sh
```
### Client-side
Clone the sherlock-tonalities repository and install dependancies
```
yarn
```
Start Tonalities
```
yarn start
```

```mermaid
graph LR
  A[Start] --> B{Error?};
```
