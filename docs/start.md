# Tonalities documentation

This website aims at presenting and documenting the Tonalities app. For more information about our team, further presentation videos, bibliographic references, a list of deliverables produced as part of the Polifonia project, please visite the [project page](https://www.iremus.cnrs.fr/fr/programme-de-recherche/tonalities) on the IReMus website.

## Goals and objectives

As part of the European project Polifonia (H2020 grant agreement N. 101004746) Tonalities embraces the open linked data paradigm to reference large corpora of music made available in digital score libraries and to explore them analytically through a quantitative-qualitative approach. This approach consists of modelling different theories – historical or contemporary, specific or general – and applying them to musical works through a dedicated interface combining machine learning and human annotation

The collaborative annotation interface for music analysis that constitutes the core of Tonalities addresses the following challenges:

Selecting different models, corresponding to different theoretical and analytical viewpoints.
Selecting every item on the score (single notes, groups of notes, verticalities, etc.) at any level of granularity.
Creating arbitrary selection trees through nested selections, editing a selection or adding/removing elements (including other selections).
Associating concepts derived from the models with these analytical elements.
Commenting on the analytical annotations.
Comparing the annotations made on the same score either by different users or on the basis of different models.
To this end, Tonalities builds on the laboratory’s data model, developed and implemented in the SHERLOCK project – a web platform for collaborative knowledge-creation processes – and provides a dynamic web application for creating, editing and deleting RDF data via a dedicated API.

We offer an easy-to-use and centralized programmatic interface for creating CIDOC-CRM-based RDF data relating du music theory and analysis.
We ensure that the data is standardized and fits to the SHERLOCK model developed at the IReMus.
We manage the access rights through the ORCID third party authentication service: all analytical annotations are signed (ORCID ID), resources can only be edited by their authors, all contributions can be retrieved through ORCID IDs
We encourage other web applications from other laboratories to use the service provided.
On this basis, it becomes possible to grasp how distinct theoretical viewpoints bring to light different – sometimes conflicting – musical properties; confront different analytical interpretations; look “inside” both theories and works; understand how both evolve in time in relation to each other; and, ultimately, provide reasoned, documented, and authored analyses. In addition to external score libraries (Josquin Research Project, CRIM, Measuring Polyphony, Gesualdo Online, The Lost Voices Project), Tonalities is relying on the NEUMA score library developed at the IReMus in collaboration with the CNAM.

## Demos

### Presentation of the annotation interface and note selection [^1]

<iframe frameBorder="0" src="https://streaming.ccsd.cnrs.fr/04/09/37/12/tonalities_V2_demo_2.mp4" width="640" height="480" allowfullscreen></iframe>

### Annotation of a cadence with its individual melodic lines and cadence steps [^2]

<iframe frameBorder="0" src="https://drive.google.com/file/d/1OUOI9tMyQs3u1H_0FBJNuaqzaTcOxtAf/preview" width="640" height="480" allowfullscreen></iframe>

### Annotation of the exposition, episodes and coda of a fugue [^3]

<iframe frameBorder="0" src="https://drive.google.com/file/d/1U4pnRmpRcBv3ZivSIt1bAQBv3qu5DECZ/preview" width="640" height="480" allowfullscreen></iframe>

[^1]:
    [Félix Poullet-Pagès, Tonalities' Interface Demo](https://hal.science/hal-04093712) [⟨hal-04093712⟩](https://hal.science/hal-04093712) : Login, score selection, project creation, presentation of the annotation interface, model selection, note selection, navigation through the annotation tree.

[^2]:
    [Christophe Guillotel-Nothmann, Nested annotations in Tonalities ](https://drive.google.com/file/d/1OUOI9tMyQs3u1H_0FBJNuaqzaTcOxtAf/view): Annotation of a cadence with its individual melodic lines and cadence steps.

[^3]:
    [Marco Gurrieri, Annotation of large selections in Tonalities](https://drive.google.com/file/d/1U4pnRmpRcBv3ZivSIt1bAQBv3qu5DECZ/view) : Annotation of the exposition, episodes and coda of a fugue.