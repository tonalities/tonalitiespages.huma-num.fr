---
hide:
  - navigation
---

# Dataset validation

We will be implementing a webservice to allow dataset imports to Tonalities triplestore.

[Open webservice :octicons-arrow-right-16:](https://tonalities.gitpages.huma-num.fr/validator){ .md-button }

For validation we will be using the SHACL API of the Interoperability Test Bed, a conformance testing service offered by the European Commission's DG DIGIT [^1].

The service will allow for bulk validation (meaning allow upload of multiple files simultaneously). The validated datasets will be sent to our team to review and upload to the public triplestore.

At the current stage, our validation conforms to the following pattern

![Tonalities data model](assets/data.svg)

```ttl
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix crm: <http://www.cidoc-crm.org/cidoc-crm/> .
@prefix iremus: <http://data-iremus.huma-num.fr/id/> .
@prefix sherlock: <http://data-iremus.huma-num.fr/ns/sherlock#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .

##############
# Shapes for crm:E7_Activity
##############

sherlock:ActivityShape a sh:NodeShape ;
    sh:targetClass crm:E7_Activity ;
    sh:property [
        sh:path crm:P2_has_type ;
        sh:hasValue iremus:21816195-6708-4bbd-a758-ee354bb84900 ;
    ] ;
    sh:property [
        sh:path sherlock:has_privacy_type ;
        sh:minCount 1 ;
    ] ;
    sh:property [
        sh:path dcterms:creator ;
        sh:minCount 1 ;
    ] ;
    sh:property [
        sh:path crm:P1_is_identified_by ;
        sh:node sherlock:ProjectNameShape ;
    ] ;
    sh:property [
        sh:path crm:P14_carried_out_by ;
        sh:minCount 1 ;
    ] ;
    sh:property [
        sh:path crm:P9_consists_of ;
        sh:node sherlock:AttributeAssignmentShape ;
    ] .

##############
# Shapes for crm:E13_Attribute_Assignment
##############

sherlock:AttributeAssignmentShape a sh:NodeShape ;
    sh:targetClass crm:E13_Attribute_Assignment ;
    sh:property [
        sh:path sherlock:has_document_context ;
        sh:nodeKind sh:IRI ;
    ] ;
    sh:property [
        sh:path dcterms:created ;
        sh:datatype xsd:string ;
    ] ;
    sh:property [
        sh:path dcterms:creator ;
        sh:nodeKind sh:IRI ;
    ] ;
    sh:or (
        [ # Case 1: crm:P177_assigned_property_of_type is crm:P67_refers_to
            sh:property [
                sh:path crm:P177_assigned_property_of_type ;
                sh:hasValue crm:P67_refers_to ;
            ] ;
            sh:property [
                sh:path crm:P140_assigned_attribute_to ;
                sh:nodeKind sh:IRI ; # Representing list of XML IDs
            ] ;
            sh:property [
                sh:path crm:P141_assigned ;
                sh:node sherlock:EntityShape ;
            ] ;
        ]
        [ # Case 2: crm:P177_assigned_property_of_type is crm:P2_has_type
            sh:property [
                sh:path crm:P177_assigned_property_of_type ;
                sh:hasValue crm:P2_has_type ;
            ] ;
            sh:property [
                sh:path crm:P140_assigned_attribute_to ;
                sh:node sherlock:EntityShape ;
            ] ;
            sh:property [
                sh:path crm:P141_assigned ;
                sh:or (
                    [ sh:datatype xsd:string ] # Arbitrary text
                    [ sh:nodeKind sh:IRI ]    # External IRI
                ) ;
            ] ;
        ]
        [ # Case 3: crm:P177_assigned_property_of_type is crm:P106_is_composed_of
            sh:property [
                sh:path crm:P177_assigned_property_of_type ;
                sh:hasValue crm:P106_is_composed_of ;
            ] ;
            sh:property [
                sh:path crm:P140_assigned_attribute_to ;
                sh:node sherlock:EntityShape ;
            ] ;
            sh:property [
                sh:path crm:P141_assigned ;
                sh:node sherlock:EntityShape ;
            ] ;
        ]
    ) .

##############
# Shapes for crm:E28_Conceptual_Object
##############

sherlock:EntityShape a sh:NodeShape ;
    sh:targetClass crm:E28_Conceptual_Object .

##############
# Shapes for crm:E41_Appellation
##############

sherlock:ProjectNameShape a sh:NodeShape ;
    sh:targetClass crm:E41_Appellation ;
    sh:property [
        sh:path crm:P190_has_symbolic_content ;
        sh:datatype xsd:string ;
    ] .
```

[Download SHACL file :material-download:](https://gitlab.huma-num.fr/tonalities/validator/-/raw/main/cli/model.ttl){ .md-button }

[^1]: Interoperability Test Bed https://interoperable-europe.ec.europa.eu/collection/interoperability-test-bed-repository/solution/interoperability-test-bed
[^2]: SHACL documentation https://www.itb.ec.europa.eu/docs/guides/latest/validatingRDF
[^3]: SHACL API https://www.itb.ec.europa.eu/shacl/swagger-ui/index.html
