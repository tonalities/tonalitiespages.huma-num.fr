# About Tonalities

## Introduction

As one of the ten pilots participating in the European project Polifonia: a Digital Harmoniser for Musical Heritage Knowledge (Horizon2020 - RIA)[^1], Tonalities embraces the open linked data paradigm to reference large corpora of music made available in digital score libraries and to explore them through a specific approach. This approach consists of modeling different theories---historical or contemporary, specific or general---and applying them to musical works through a dedicated collaborative annotation interface. This interface helps to grasp how distinct theoretical viewpoints bring to light different, sometimes conflicting, musical properties; to confront different analytical interpretations; and, ultimately, to provide an reasoned, documented and authored modal-tonal classification of musical pieces.

## State of the Art

Tonalities builds on previous studies that relate the musical system to
its history[^2], grant early music other
conditions of truth than tonality [^3], and
utilize statistical methods[^4]. The
annotation interface, which is one of the cornerstones of the project,
takes its cue from previous projects that use a score visualization
interface to produce musical knowledge: The Josquin Research
Project[^5], which provides a large corpus of
scores and analytical routines for their exploration; The Lost Voices
Project[^6], which opens an avenue for
collaborative analysis; and Dezrann[^7], which provides ergonomic
tools for score annotation. Tonalities complements these initiatives on
several levels through leveraging the semantic annotation and open
linked data paradigms: the modeling of music concepts into OWL
ontologies to make analytical assumptions explicit and to permit
choosing among different theoretical perspectives; the collaborative
dimension to trace each critical annotation and to comment and compare
it with others; the addressability of all resources: models, scores,
analytical annotations, critical comments.

## A Collaborative Annotation Interface that Draws on Knowledge Graphs Relating to Music, Music Theory, and Cultural Heritage

Through its annotation interface, Tonalities (a) makes use of
theoretical models, which (b) can be associated with arbitrary
selections on the score and (c) lead to critical analyses through
collaborative approaches.

1.  **Modelling different theories relating to the same knowledge
    domain.** The modelling of different theories relating to the same
    knowledge domain---i.e. modal-tonal organization---aims to avoid the
    limitations of a single "standard" or "global" theoretical
    perspective, arbitrarily chosen and applied mechanically despite its
    limited validity (belonging to a historical period, restricted to
    certain repertoires, conveying certain ideologies, etc.). Instead,
    Tonalities applies different models---corresponding to distinct
    theoretical viewpoints---to a single musical work, allowing the user
    to observe how these perspectives affect its modal-tonal structure.
    Further, this multifaceted approach allows for the confrontation of
    models in order to grasp how theoretical concepts evolve over time
    and space. The alignment of these ontologies then shows how concepts
    (such as "mode", "D Ionian", "transposition", "cadence", "chord",
    etc.) evolve while the terms to denote them remain constant and,
    conversely, how certain concepts remain identical while their
    signifiers change. At the present stage of development, two
    historical theories are operational: Zarlino
    1558[^8] and Praetorius
    1619[^9] (each with circa 2500
    axioms, 500 classes, and 100 object
    properties)[^10][^11]. The Praetorius
    model has been presented[^12] and
    applied[^13] [^14] in
    former studies. Initial tests have been undertaken over the last 12
    months to infer the mode of individual works from these models.
    Further, a set of functions has been developed to allow the
    comparison of the models, both at the class level and at the
    property level, and to measure their differences and similarities
    (see Figure [\[models\]](##models){reference-type="ref"
    reference="models"});

    ![image](assets/1.png){width="\\linewidth"}

2.  **Selecting arbitrary selections in the score and associating them
    with concepts.** Once a musical element---ranging from a single note
    to complex segments (i.e. not contiguous or nested)---has been
    selected, it can be associated with an analytical property and/or
    concept from a theoretical model. This association is fully
    recursive: a parent concept, corresponding to a main selection, can
    itself contain child concepts that refer to subsets. The linking of
    concepts with selections can be done manually and/or via knowledge
    extraction routines either based on deep learning (for root and
    cadence analysis) or formalized approaches (for voice range and key
    analysis, etc.);

3.  **Commenting analytical annotations in the context of collaborative
    approaches.** Analytical annotations can give rise to critical
    comments. These comments are signed and addressable. Their purpose
    is to correct and qualify and/or contextualize the analytical
    information, particularly in the context of collaborative
    approaches. This collaborative dimension is at the heart of the
    interface's methodological ambitions. First, Tonalities aims to rely
    on collaborative work for the annotation and analytical exploration
    of large corpora. The aim is to overcome isolated studies on
    individual musical works in favour of team projects predisposed to
    discern properties and trends of the musical language in large
    collections. Second, the collaborative work aims to contextualize
    analytical statements through argumentation, refutation, and
    comparison. The challenge is to go beyond agnostic or positivist
    postures that would see in a musical analysis objective and
    universally valid facts in favour of a dialectical activity which
    places at its centre the context-depend viewpoint of analysts. The
    Tonalities interface has therefore also been designed in view of
    identifying and applying methodological paradigms in which AI and
    manual (collaborative) annotation are articulated in approaches that
    remain sensitive to the challenges within the humanities and social
    sciences, in particular those of music analysis[^15].

    ![image](assets/2.png){width="\\linewidth"}

To uphold this practice, Tonalities consumes three types of semantic
data:

1.  MEI scores are fully converted to RDF triples: each XML element
    receives a unique IRI; each XML attribute gives birth to an RDF
    triple; and basic CIDOC-CRM patterns are used to express the type
    (note, rest, verse, etc.), the id (reflected in the IRI of the
    corresponding semantic data), and the nesting of each element.
    However, some analytical tasks assume the ability to address
    observables that are not represented by musical signs, such as (a)
    score offsets (verticalities), (b) note offsets, and (c) arbitrary
    selections of MEI elements. Score and note offsets are extracted
    with Music21 libraries, and free selections are modeled as basic
    CIDOC-CRM patterns. Each non-MEI observable also receives a unique
    IRI.

2.  The project formalizes theoretical knowledge (either historical
    sources or contemporary theory) in the form of OWL ontologies. These
    ontologies provide concepts and properties which, imported into the
    Tonalities application, provide an analytical vocabulary to annotate
    scores.

3.  Tonalities allows to display and compare two types of semantic
    annotations linking together analytical observables and theoretical
    concepts: those created manually in the interface and those
    generated by programs relying on machine learning and formalized
    knowledge production rules algorithms. Each annotation is expressed
    as a CIDOC-CRM event, is thus dated and signed (by a human
    musicologist or by a specific algorithm), and takes the form of a
    reification of a RDF triple. An annotation may be simple, in which
    case the object/body is a single value (such as a dissonance type
    associated to a subject/target representing a note). Annotations may
    also be complex, for example when an analyst describes the inner
    structure of a cadence. In such cases, a macro-selection may be
    annotated with cadence types ("DiminishedCadence", "PerfectCadence",
    etc.) and may contain sub-selections annotated through the
    attribution of properties ("hasAltizans", "hasCantizans", etc).
    Within each of these sub-selections, each note may in turn be
    annotated with its own properties ("hasAntepenultima",
    "hasPenultima", etc.).

## Challenges and Working Perspectives

Tonalities' interface is currently available in an alpha version.
Several improvements and developments are in progress, including
ergonomics, metadata treatment, musical queries, download/upload options
for end users, etc. In the medium-term, the interface will provide means
for the fine-grained comparison of concurrent annotations made on a
score. Diagrams will also be implemented for the diachronic and
synchronic analysis of different musical features, including pitch
collections, harmonic progressions, cadences, etc. The long-term goal is
to supply an extended palette of analytical symbols for score
annotation, notably for the purpose of harmonic analysis (Roman
numerals, Riemannian functions, or harmonic
vectors[^4]).

[Download PDF :material-download:](https://hal.science/hal-03923731/){ .md-button }

[^1]: Polifonia, 2021. URL: https://polifonia-project.eu.
[^2]:
    C. Dahlhaus, Untersuchungen über die Entstehung
    der harmonischen Tonalität, Bärenreiter, 1968.

[^3]:
    F. Wiering, The Language of the Modes: Studies in
    the History of Polyphonic Modality, Criticism and
    Analysis of Early Music, Routledge, 2001.

[^4]:
    N. Meeùs, Vecteurs harmoniques, Musurgia 10
    (2003) 7–34.

[^5]:
    The Josquin Research Project, 2010. URL: https://
    josquin.stanford.edu.

[^6]:
    The Lost Voices Project, 2012. URL: http://
    digitalduchemin.org.

[^7]: Dezrann, 2019. URL: http://www.dezrann.net.
[^8]:
    G. Zarlino, Le Istitutioni harmoniche, The author,
    Venezia, 1558.

[^9]:
    M. Praetorius, Syntagma musicum. Tomus tertius,
    Michael Praetorius, Wolfenbüttel, 1619.

[^10]:
    C. Guillotel-Nothmann, modalityTonality
    Zarlino 1558, https://raw.githubusercontent.com/
    polifonia-project/modal-tonal-ontology/main/
    historicalModels/modalityTonality_Zarlino_1558.
    rdf, 2022.

[^11]:
    C. Guillotel-Nothmann, modalityTonality Praeto-
    rius 1619, https://github.com/polifonia-project/
    modal-tonal-ontology/blob/main/historicalModels/
    modalityTonality_Praetorius_1619.owl, 2021.

[^12]:
    C. Guillotel-Nothmann, Knowledge Extraction and
    Modelling in the Project Thesaurus Musicarum Ger-
    manicarum, 2020. Prague DH Workshops Session
    III.

[^13]:
    A.-E. Ceulemans, C. Guillotel-Nothmann, Praeto-
    rius’ Polyhymnia Caduceatrix (1619): Diatonische
    Logik und Skalenlogik in mehrchörigen und konz-
    ertierenden Werken des Frühbarocks, Zeitschrift
    der Gesellschaft für Musiktheorie, Gesellschaft für
    Musiktheorie 17 (2020) 51–93.

[^14]:
    C. Guillotel-Nothmann, A.-E. Ceulemans, Das
    diatonisch-chromatische System zur Zeit des
    Michael Praetorius. Eine digitale Neuerschließung
    des Syntagma Musicum (1619) in Verbindung mit
    dem Tanzzyklus Terpsichore (1612), Harrassowitz,
    2021, pp. 109–149.

[^15]:
    C. Guillotel-Nothmann, Les signes musicaux et leur
    étude par l’informatique. le statut épistémologique
    du numérique dans l’appréhension du sens et de la
    signification en musique, Revue musicale OICRM
    6 (2020) 45–72.
