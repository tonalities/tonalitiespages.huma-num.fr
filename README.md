### Source repository for Tonalities documentation live at [tonalities.gitpages.huma-num.fr](https://tonalities.gitpages.huma-num.fr)

Create environment and install dependencies
```
python3 -m venv venv
pip install mkdocs-material
```

Start the local server
```
source venv/bin/activate
mkdocs serve
```
